<?php
$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'homepage';
//aggiungere la cosa per prendere solo l'azone prima della &cd
//echo "<hr>Action=$action<hr>";


/* equivalente - idea Daniel C.
$azioniPossibili=['homepage','login','login-check'];
if (is_array($action,$azioniPossibili)) {
    require('../src/controller/' . $action . '.php');
}
*/
switch ($action){
    case 'homepage':
        require('../src/controller/homepage.php');
        break;
    case 'login':
        require('../src/controller/Access.php');
        login();
        break;
    case 'login-check':
        require('../src/controller/Access.php');
        loginCheck();
        break;
    case 'logout':
        require('../src/controller/Access.php');
        logout();
        break;
    case 'change-password':
        require('../src/controller/Password.php');
        showForm();
        break;
    case 'set-password':
        require('../src/controller/Password.php');
        setPassword();
        break;
    case 'user-list':
        require('../src/controller/User.php');
        showUserList();
        break;
    case 'edit-form':
        require('../src/controller/User.php');
        showEditUserForm();
        break;

    case 'add-form':
        require('../src/controller/User.php');
        showAddUserForm();
        break;
    case 'edit-user':
        require('../src/controller/User.php');
        edit();
        break;
    case 'delete-user':
        require('../src/controller/User.php');
        delete();
        break;
    case 'add-user':
        require('../src/controller/User.php');
        add();
        break;
}