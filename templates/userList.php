<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Elenco</title>
</head>
<body>
<?php

global $db;
$sql="SELECT * FROM app_user ";
$result = $db->execute($sql);

$page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;

$len = count($result->getRows());
$maxPage = (int)($len / 10);
if ($page > $maxPage) {
    $page = $maxPage;
}
if ($page < 1) {
    $page = 1;
}

?>

<div class="container">

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">username</th>
            <th scope="col">password</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;



        while($row = $result->fetch_assoc()){

            $i++;
            if ($i <= ($page - 1) * 10) {
                continue;
            }
            if ($i > ($page) * 10) {
                continue;
            }
            ?>
        <tr>
            <th scope="row"><?php echo $row['username'] ?></th>
            <td><?php echo $row['password'] ?></td>

            <td>
                <a href="index.php?action=edit-form&id=<?php echo $row['id'] ?>" class="btn btn-primary btn-sm">Modifica</a>
            </td>

            <td>
                <a href="index.php?action=delete-user&id=<?php echo $row['id'] ?>" class="btn btn-danger btn-sm">Cancella</a>
            </td>
        </tr>

        </tbody>
    </table>


    <div class="row">
        <div class="col">
            <a href="index.php?action=add-form" class="btn btn-success">Add</a>
        </div>

        <div class="col">

            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php
                    $start = max($page - 2, 1);
                    for ($i = $start; $i < min($start + 5, $maxPage); $i++) { ?>
                        <li class="page-item <?php echo ($i == $page) ? 'active' : '' ?>"><a class="page-link"
                                                                                             href="?page=<?php echo $i; ?>">
                                <?php echo $i; ?>
                            </a></li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>


